package controllers

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/noelruault/weatherapp/internal/models"
	"github.com/sirupsen/logrus"
)

// GetByCity returns a formatted JSON with weather data for a given city
//
// GET /v1/weather/?city=sydney
func GetByCity(c *gin.Context) {
	var weather = models.NewWeather()

	city := c.Query("city")
	if city == "" {
		c.JSON(http.StatusNotFound, gin.H{"error": "not_found"})
		return
	}

	weather.City = city
	err := models.GetByCity(&weather)
	if err != nil {
		logrus.Errorf("models: GetByCity %d", err)
		c.JSON(http.StatusInternalServerError, gin.H{"error": "internal_error"})
		return
	}

	c.JSON(http.StatusOK, &weather)
}
