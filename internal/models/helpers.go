package models

import (
	"encoding/json"
	"math"
	"net/http"
)

func getJSON(cli *http.Client, url string, target interface{}) error {
	r, err := cli.Get(url)
	if err != nil {
		return err
	}
	defer r.Body.Close()

	return json.NewDecoder(r.Body).Decode(target)
}

// MSec2KmH converts a given float64 from meters/sec to km/hour and rounds the
// number to two decimal places.
func MSec2KmH(s float64) float64 {
	return math.Round((s*18)/5*100) / 100
}
