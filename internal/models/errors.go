package models

// These errors are returned by the models and can be used to provide error
// codes to the API results.
const (
	ErrServiceUnavailable ModelError = "bad_gateway, not receiving a valid response from the client api servers"
)

// ModelError defines errors exported by this package.
type ModelError string

// Error returns the exact original message of the e value.
func (e ModelError) Error() string {
	return string(e)
}
