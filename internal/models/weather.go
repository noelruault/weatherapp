package models

import (
	"net/http"
	"time"

	"github.com/sirupsen/logrus"
)

// type ModelServices struct {
// 	ws *Weather
// }

// func NewModelServices() *Weather {
// 	ms := ModelServices{
// 		ws: Weather,
// 	}
// 	return &ms
// }

// Wind data contained in the Weather struct
type Wind struct {
	Speed float64 `gorm:"type:bigint;not null" json:"wind_speed"`
}

// Temperature data contained in the Weather struct
type Temperature struct {
	Base float64 `gorm:"type:bigint;not null" json:"temperature_degrees"`
}

// Weather is used as a homogeneous structure to store the data of several clients
type Weather struct {
	City string `gorm:"unique;size:255;not null" json:"-"`
	Temperature
	Wind
}

// NewWeather creates a new Weather value with default field values applied.
// This could be used in a future to initialize the Weather by giving default
// values as city or date.
func NewWeather() Weather {
	return Weather{}
}

// GetByCity makes requests to multiple clients to fetch the current weather
// of a given city
func GetByCity(w *Weather) error {
	var err error

	ow := NewProvider(`http://api.openweathermap.org/data/2.5/weather?q=` + w.City + `&appid=` + openWeatherAPIKey + `&units=metric`)
	ws := NewProvider(`http://api.weatherstack.com/current?access_key=` + weatherStackAPIKey + `&query=` + w.City)

	err = ow.OpenWeatherMapAPI(w)
	if err != nil {
		logrus.Errorf("OpenWeather API had errors on request %d", err)
		err = ws.WeatherStack(w)
		if err != nil {
			logrus.Errorf("OpenWeather API had errors on request %d", err)
			return ErrServiceUnavailable
		}
	}

	return nil
}

type Provider struct {
	URL        string
	httpClient *http.Client
}

func NewProvider(url string) *Provider {
	p := Provider{
		URL: url,
		httpClient: &http.Client{
			Timeout: 3 * time.Second,
		},
	}

	return &p
}

// ---------------- CLIENTS --------------

// ---------------- CLIENT1 --------------
const (
	openWeatherAPIKey = "2326504fb9b100bee21400190e4dbe6d"
)

type oWMain struct {
	Temp float64 `json:"temp"`
}

type oWWind struct {
	Speed float64 `json:"speed"`
}

// openWeatherMap builds a struct that contains data retrieved from
// api.openweathermap.org regarding the weather.
type openWeatherMap struct {
	Main oWMain `json:"main"`
	Wind oWWind `json:"wind"`
}

// OpenWeatherMapAPI makes a request to api.openweathermap.org using a city
// stored in the Weather struct provided and an API Key which is set as global
// variable.
func (p *Provider) OpenWeatherMapAPI(w *Weather) error {
	ow := new(openWeatherMap)

	err := getJSON(p.httpClient, p.URL, ow)
	if err != nil {
		return err
	}

	w.Temperature.Base = ow.Main.Temp
	w.Speed = MSec2KmH(ow.Wind.Speed)

	return nil
}

// ---------------- CLIENT2 --------------

const (
	weatherStackAPIKey = "dbd0e9da4292d03d4e684d83dd543bec"
)

type wSCurrent struct {
	Temp      float64 `json:"temperature"`
	WindSpeed float64 `json:"wind_speed"`
}

// openweathermap builds a struct that contains data retrieved from
// api.openweathermap.org regarding the weather.
type weatherStack struct {
	Current wSCurrent `json:"current"`
}

// WeatherStack makes a request to api.openweathermap.org using a city
// stored in the Weather struct provided and an API Key which is set as global
// variable.
func (p *Provider) WeatherStack(w *Weather) error {
	ws := new(weatherStack)

	err := getJSON(p.httpClient, p.URL, ws)
	if err != nil {
		return err
	}

	w.Temperature.Base = ws.Current.Temp
	w.Speed = ws.Current.WindSpeed

	return nil
}

// ---------------- /CLIENTS --------------
