package models

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"golang.org/x/xerrors"
)

func TestWeather_GetByCity(t *testing.T) {

	var cases = []struct {
		name       string
		weather    *Weather
		outweather *Weather
		outerr     error
		setup      func(t *testing.T)
	}{
		{
			"openWeatherMapAPI",
			&Weather{City: "-wrongCity-"},
			&Weather{
				City:        "-wrongCity-",
				Temperature: Temperature{Base: 0},
				Wind:        Wind{Speed: 0},
			},
			nil,
			nil,
		},
	}

	for _, cs := range cases {
		t.Run(cs.name, func(t *testing.T) {
			if cs.setup != nil {
				cs.setup(t)
			}

			err := GetByCity(cs.weather)

			if cs.outerr != nil {
				assert.Error(t, err)
				assert.True(t, xerrors.Is(err, cs.outerr),
					"errors must match, expected %v, got %v", cs.outerr, err)

			} else {
				assert.NoError(t, err)
				assert.Equal(t, cs.outweather, cs.weather)
			}
		})
	}

}
