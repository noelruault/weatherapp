package app

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"github.com/gin-contrib/cache"
	"github.com/gin-contrib/cache/persistence"
	"github.com/gin-gonic/gin"
	"github.com/noelruault/weatherapp/internal/controllers"
)

type webServer struct {
	eng    *gin.Engine
	server http.Server
}

func newWebServer(port string) *webServer {
	var ws = &webServer{}

	ws.setupRoutes()
	ws.server = http.Server{
		Addr:         ":" + port,
		Handler:      ws.eng,
		ReadTimeout:  10 * time.Second,
		WriteTimeout: 10 * time.Second,
	}

	return ws
}

func (ws *webServer) Run() error {
	err := ws.server.ListenAndServe()
	if err != nil {
		if err == http.ErrServerClosed {
			return nil
		}

		return fmt.Errorf("webServer.Run %d", err)
	}

	return nil
}

func (ws *webServer) Shutdown() error {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	err := ws.server.Shutdown(ctx)
	if err != nil {
		return fmt.Errorf("webServer.Shutdown %d", err)
	}

	return nil
}

func (ws *webServer) setupRoutes() {
	gin.SetMode(gin.ReleaseMode)
	mux := gin.New()
	mux.Use(gin.Recovery())

	apimux := mux.Group("/v1/")
	// apimux := restricted.Group("/v1/")
	ws.setupWeatherRoutes(apimux)

	ws.eng = mux
}

func (ws *webServer) setupWeatherRoutes(mux *gin.RouterGroup) {
	store := persistence.NewInMemoryStore(3 * time.Second)
	mux.GET(
		"/weather",
		cache.CachePage(store, 3*time.Second, func(c *gin.Context) {
			// c.String(200, "cache: "+fmt.Sprint(time.Now().Unix())) // uncomment to see how acts the cache in temporary value
			controllers.GetByCity(c)
		}),
	)
}
