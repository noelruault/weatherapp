package app

import (
	"flag"
	"net/http"
	"os"
	"testing"
	"time"

	"golang.org/x/xerrors"
)

const (
	testPort = "59999"
	testURL  = "http://localhost:59999"
)

var app App

func TestMain(m *testing.M) {
	// setup app
	app.Configure(&Config{
		Port: testPort,
	})

	// start running the full application
	go func() {
		app.Run()
	}()

	// prepare things
	isServerUp()

	// runs the tests
	flag.Parse()
	ret := m.Run()

	// tear down the application
	app.Shutdown()
	os.Exit(ret)
}

func isServerUp() {
	var err error
	for i := 6; i > 0; i-- {
		_, err = http.Get(testURL + "/api/v1/weather/")
		if err == nil {
			return
		}

		time.Sleep(1 * time.Second)
	}

	panic(xerrors.Errorf("attempted to connect to test app, did not succeed: %w", err))
}
