package app

import (
	"bytes"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/noelruault/weatherapp/internal/models"
	"github.com/stretchr/testify/assert"
)

type testWeatherService struct {
	getByCity func(w *models.Weather) error
}

func (t *testWeatherService) GetByCity(mw *models.Weather) error {
	if t.getByCity != nil {
		return t.getByCity(mw)
	}

	panic("not provided")
}

func TestWeather_GetByCity(t *testing.T) {
	gin.SetMode(gin.TestMode)
	ws := &testWeatherService{}

	mux := gin.New()
	mux.GET("/v1/weather/", GetByCity)

	var cases = []struct {
		name      string
		path      string
		outStatus int
		outJSON   string
		setup     func(t *testing.T)
	}{
		{
			"notFound",
			"?city=test1234",
			http.StatusOK,
			`{"temperature_degrees":0, "wind_speed":0}`,
			nil,
		},
		{
			"badContent",
			"?sydney",
			http.StatusNotFound,
			`{"error":"not_found"}`,
			nil,
		},
	}

	for _, cs := range cases {
		t.Run(cs.name, func(t *testing.T) {
			w := httptest.NewRecorder()
			c, _ := gin.CreateTestContext(w)
			c.Request, _ = http.NewRequest("GET", "/v1/weather/"+cs.path,
				bytes.NewReader([]byte(``)))

			if cs.setup != nil {
				cs.setup(t)
			}

			mux.HandleContext(c)

			res := w.Result()
			assert.Equal(t, cs.outStatus, res.StatusCode)
			assert.Contains(t, res.Header.Get("Content-Type"), "application/json")
			assert.JSONEq(t, cs.outJSON, w.Body.String())

			*ws = testWeatherService{}
		})
	}
}
