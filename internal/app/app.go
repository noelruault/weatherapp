// Package app implements the tickets service features.
package app

import (
	"fmt"

	"github.com/sirupsen/logrus"
)

// App contains all the application dependencies required by the handlers and other
// methods, as well as general configuration.
type App struct {
	// webserver is the HTTP server for weatherapp.
	webServer *webServer
}

// Config contains settings used to instantiate an App when calling its Configure method.
type Config struct {

	// Port is the HTTP server port number as a string.
	Port string
}

// Configure sets the application parameters in the internal struct value. The function will
// start and test the database connection. The port may be optionally set to the TCP port where the HTTP server
// should listen, the default being 8000.
func (a *App) Configure(c *Config) error {
	err := c.check()
	if err != nil {
		return fmt.Errorf("App.Configure %d", err)
	}

	// configure services
	a.webServer = newWebServer(c.Port)

	return nil
}

// Run starts serving the HTTP routes configured with an App object.
// The server listens on port 8000 by default.
func (a *App) Run() error {
	const serviceCount = 1
	err := make(chan error, serviceCount)

	go func() {
		logrus.WithField("addr", a.webServer.server.Addr).Info("HTTP server starts")
		err <- a.webServer.Run()
	}()

	var i int
	for e := range err {
		if e != nil {
			return fmt.Errorf("error when running application services %d", e)
		}

		i++
		if i == serviceCount {
			break
		}
	}

	return nil
}

// Shutdown gracefully stops all server services so the process can terminate.
func (a *App) Shutdown() error {
	errWS := a.webServer.Shutdown()

	if errWS != nil {
		return fmt.Errorf("could not stop the webserver service %d", errWS)
	}

	return nil
}

// check verifies that the configuration is valid. It may also set default
// values for fields left undefined.
func (c *Config) check() error {
	if c.Port == "" {
		c.Port = "8000"
	}

	return nil
}
