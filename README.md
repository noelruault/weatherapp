Go Backend project
==================

Create an HTTP Service that reports on Sydney weather. This service will source its information from either of the below providers:

    1. weatherstack (primary):
        Documentation: https://weatherstack.com/documentation

    2. OpenWeatherMap (failover):
        Documentation: https://openweathermap.org/current

Notes:

    The service should return a JSON payload with a unified response containing temperature in degrees Celsius and wind speed. If one of the provider goes down, your service can quickly failover to a different provider without affecting your customers.

    Weather results are fine to be cached for up to 3 seconds on the server in normal behaviour to prevent hitting weather providers. Those results must be served as stale if all weather providers are down.

    Have scalability and reliability in mind when designing the solution. The proposed solution should allow new developers to make changes to the code safely.

Review:

    * Clear responsibility per component
    * Documentation and communicate around trade-offs you might have made, anything you left out, or what you might do differently if you were to spend additional time on the task.
    * Split unit tests mocking providers vs integration test talking to those provider

    * Review unused and inconsistency such as copy/paste from ratings app
    * We know it takes time so we are happy if you focus on just one part of the app it is fine

For this, we will design an unique GET endpoint which will take care of retrieving the weather for a given city.

The GET method is used to retrieve a (weather) resource. A body will not be passed with a GET request.

Get
---

Returns a specific weather's details for a given city.

**Request:**

    GET /v1/weather/?city={city}

The **city** parameter value refers to the name of the city which weather wants to be queried.

**Response:**

    HTTP/1.1 200 OK
    Content-Type: application/json

    {
        "temperature_degrees": 14.81,
        "wind_speed": 9.36
    }

Reponse codes:

* **200**: Request completed successfully.
* **400**: The request could not be understood or has validation errors.
* **404**: Requested city not found.

Error example:

    HTTP/1.1 404 Not Found
    Content-Type: application/json
    Cache-Control: no-store
    Pragma: no-cache

    {
        "error": "not_found"
    }

| Case | HTTP code | error | fields |
| - | - | - | - |
| City parameter value is invalid | 400 | validation_error | |
| Item could not be found | 404 | not_found | |
| Internal error | 500 | server_error | |

Architectural Design
--------------------

Model-View-Controller will be used as the architectural pattern to help organize the a application by separating code based on what it is responsible for, in order to fit the specification and handle a clear responsibility per component.

We won't be using views in this exercise but is also a good choice to organize an application of this style and also will give us the possibility to integrate views afterwards if needed.

__*Controllers*__ will be used to handle most of the business logic that happens behind the scenes for a web request. Will use models to do these things.
__*Models*__ will be responsible for interacting with the raw data. What means interacting with data that comes from other services or APIs.

Design principles
-----------------

in order to make this application comply with the standards of software design, the SOLID principles will be followed.

Regarding the "Single Responsibility Principle", every piece of code should have only a single responsibility and we only have one reason to change or modify it's content: "Do one thing and do it well".

This is the reason this project is quite strict with the package naming and the funtionality of it's components. Having the next structure:

// TODO PICTURE HERE

Thus, the controllers, models and clients (in the end functions and methods) that will be used throughout the exercise have been designed trying to find a balance between coupling and cohesion.

// TODO:  ? the rest of the principles can be conceived, analyzing the application code.
// TODO: Interface Segregation Principle

Check more about design principles and pattens here: [Design Principles and Design Patterns, Robert C. Martin, (2000)](https://fi.ort.edu.uy/innovaportal/file/2032/1/design_principles.pdf), [Dave Cheney, Solid Go Design, (2016)](https://dave.cheney.net/2016/08/20/solid-go-design)

Trade-Offs (and Strengths?) // TODO
----------

* Documentation and communicate around trade-offs you might have made, anything you left out, or what you might do differently if you were to spend additional time on the task.

TODO
----

* Estaba creando servicios tanto para el controlador como para el modelo, para que en un futuro fuese más sencillo utilizarlos, en el caso en el que se implementase una base de datos. Era completamente innecesario (???).
* Hay que hacer tests unitarios y de integracion, tambien mockear.
* Se puede hacer un balanceador mas sofisticado para lo de las peticiones de las APIs?
* Arreglar los tests, que hacer para mockear?

* Preguntar por el cache, y por como aislar los proveedores a otra carpeta (fuera de modelos), si fuese necesario..