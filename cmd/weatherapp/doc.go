/*
HTTP server that provides the Weather App API.

Configuration is passed as environment variables. The following ones are
available:
		PORT:
			mandatory, network port on which to serve the REST API.
*/
package main
